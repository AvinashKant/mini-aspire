NOTE: All the codes are present in their respective folder. Like tests are present in /test directory.

## Step 1: Setup app

Use this command to clone project in local system

git clone https://gitlab.com/AvinashKant/mini-aspire.git

After cloning git repository go to project directory cd mini-aspire
## Step 2: Run database and seeder

1. Create a new database 'mini-aspire' in your local db
2. Set your local db details in .env files
3. php artisan migrate
4. php artisan db:seed

## Step 3: Run local server

php artisan serve

## Step 4: Import postman collection

1. Hit Step 1 get token api
2. Hit Step 2 api to request loan api with auth token(You will get this on point 1.)
3. Hit Step 3 api to get all loan requested by user.
4. Hit Step 4 Api to get loan schedule (Pass data->[0]->id loan id path param)
5. Hit Step 5 Api to get to pay loan schedule (Pass data->[0]->user_loan_id , data->[0]->id in path param)

## If you want to run test cases

php artisan test

