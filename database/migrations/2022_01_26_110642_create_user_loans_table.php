<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->decimal('amount', 10, 2);
            $table->unsignedTinyInteger('term')->comment('1-week 2-MONTH, 3-YEAR');
            $table->unsignedTinyInteger('term_limit')->comment('If term is 1 and term_limit is 4, then the loan is for 4 months');
            $table->unsignedTinyInteger('status')->comment('1-PENDING, 2-APPROVE, 3-REJECTED');
            /**
             * Will be managed by the Admin when he approve the loan
             */
            $table->date('loan_end_date')->nullable();
            $table->decimal('balance_amount', 10, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loans');
    }
}
