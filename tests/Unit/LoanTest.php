<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\UserLoan;
use App\Models\UserLoanWeeklySchedule;
use Facades\App\Services\UserLoanService;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanTest extends TestCase
{
    use WithFaker;

    public $testUser;
    public function setUp(): void
    {
        parent::setUp();
        $this->testUser = User::factory()->create();
    }

    public function testCreateLoanRequest()
    {
        $amount = $this->faker->randomDigit();
        $term = $this->faker->numberBetween(1, 3);
        $termLimit = $this->faker->numberBetween(1, 99);
        $userLoan = UserLoanService::createLoanRequest($this->testUser, $amount, $term, $termLimit);

        $this->assertEquals($userLoan->amount, $amount);
        $this->assertEquals($userLoan->term, $term);
        $this->assertEquals($userLoan->term_limit, $termLimit);
    }

    public function testGetLoanEndDate()
    {
        $weekValue = UserLoanService::getLoanEndDate(UserLoan::WEEKLY, 1);
        $monthValue = UserLoanService::getLoanEndDate(UserLoan::MONTHLY, 1);
        $yearValue = UserLoanService::getLoanEndDate(UserLoan::YEARLY, 1);

        $this->assertEquals(now()->addWeeks(1)->format('Y-m-d'), $weekValue);
        $this->assertEquals(now()->addMonths(1)->format('Y-m-d'), $monthValue);
        $this->assertEquals(now()->addYears(1)->format('Y-m-d'), $yearValue);
    }

    public function testCreateLoanWeeklySchedule()
    {
        $amount = $this->faker->randomDigit();
        $term = $this->faker->numberBetween(1, 3);
        $termLimit = $this->faker->numberBetween(1, 99);
        $userLoan = UserLoanService::createLoanRequest($this->testUser, $amount, $term, $termLimit);

        $amount = mt_rand(1111, 9999);
        $numberOfWeeks = $this->faker->numberBetween(1, 6);

        UserLoanService::createLoanWeeklySchedule($this->testUser, $userLoan->id, $amount, $numberOfWeeks);

        $userWeeklyData = UserLoanWeeklySchedule::where(['user_id' => $this->testUser->id, 'user_loan_id' => $userLoan->id])->get();

        $this->assertEquals($userWeeklyData->count(), $numberOfWeeks);
        $this->assertEquals($userWeeklyData->first()->amount, ($amount / $numberOfWeeks));
    }
}
