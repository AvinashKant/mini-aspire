<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanTest extends TestCase
{
    use WithFaker;
    //use RefreshDatabase;

    public function test_request_loan()
    {
        $user = User::factory()->create();
        $token = $user->createToken('Login Token')->plainTextToken;

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->post('api/user/loan', [
            'amount' => $this->faker->randomDigit(),
            'term' => $this->faker->numberBetween(1, 3),
            'term_limit' => $this->faker->numberBetween(1, 99),
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'status',
                'data' => [],
                'meta' => [],
            ])
            ->assertJsonPath('message', 'Your loan request has been accepted')
            ->assertJsonPath('status', 'success');
    }

    public function test_get_user_loans()
    {
        $user = User::factory()->create();
        $token = $user->createToken('Login Token')->plainTextToken;

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])
            ->get('api/user/loan');

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
                'status',
                'data' => [],
                'meta' => [],
            ])
            ->assertJsonPath('message', 'Response given successfully.')
            ->assertJsonPath('status', 'success');
    }
}
