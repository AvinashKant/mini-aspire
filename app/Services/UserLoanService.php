<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserLoan;
use App\Models\UserLoanWeeklySchedule;

class UserLoanService
{

    /**
     * Function will lock user loan request
     *
     * @param User $user
     * @param float $loanAmount
     * @param integer $term
     * @param integer $termLimit
     * @param integer $status
     * @return UserLoan
     */
    public function createLoanRequest(User $user, float $loanAmount, int $term, int $termLimit, int $status = UserLoan::PENDING)
    {
        return $user->loans()->create([
            'amount' => $loanAmount,
            'term' => $term,
            'term_limit' => $termLimit,
            'status' => $status,

            /**
             * Not creating admin approve feature that is why adding it here
             * In future versions  when admin approve the loan then these two values will be updated
             */
            'loan_end_date' => $this->getLoanEndDate($term, $termLimit),
            'balance_amount' => $loanAmount,
        ]);
    }

    /**
     * function will return loan end date based on loan term
     *
     * @param integer $term
     * @param integer $termLimit
     * @return date
     */
    public function getLoanEndDate(int $term, int $termLimit)
    {
        $date = now();
        switch ($term) {
            case UserLoan::WEEKLY:
                $date->addWeeks($termLimit);
                break;
            case UserLoan::YEARLY:
                $date->addYears($termLimit);
                break;
            case UserLoan::MONTHLY:
            default:
                $date->addMonths($termLimit);
                break;
        }
        return $date->format('Y-m-d');
    }

    /**
     * Function will create user weekly loan schedule
     * Will be used when admin approve the loan application
     *
     * @param User $user
     * @param integer $loanId
     * @param float $loanAmount
     * @param integer $numberOfWeeks
     * @return void
     */
    public function createLoanWeeklySchedule(User $user, int $loanId, float $loanAmount, int $numberOfWeeks)
    {
        $weeklyAmount = ($loanAmount / $numberOfWeeks);
        for ($i = 1; $i <= $numberOfWeeks; $i++) {
            $insertArray[] = [
                'user_id' => $user->id,
                'user_loan_id' => $loanId,
                'amount' => $weeklyAmount,
                'due_date' => now()->addWeeks($i)->format('Y-m-d'),
            ];
        }
        UserLoanWeeklySchedule::insert($insertArray);
    }

}
