<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Traits\ResponseAPI;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ResponseAPI;

    public function getToken(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return $this->successResponse(
                [
                    'token' => auth()->user()->createToken('Login Token')->plainTextToken,
                    'user' => auth()->user(),
                ],
                trans('messages.login.success'));
        }
        return $this->errorResponse(trans('messages.login.failed'));
    }
}
