<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\LoanRequest;
use App\Models\UserLoanPayment;
use App\Models\UserLoanWeeklySchedule;
use App\Traits\ResponseAPI;
use Facades\App\Services\UserLoanService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserLoanController extends Controller
{
    use ResponseAPI;

    public function requestLoan(LoanRequest $lReq)
    {
        try {
            DB::beginTransaction();
            $loanObj = UserLoanService::createLoanRequest(Auth::user(), $lReq->input('amount'), $lReq->input('term'), $lReq->input('term_limit'));

            /**
             * Not creating admin approve feature that is why adding it here
             */

            $loanEndDate = UserLoanService::getLoanEndDate($lReq->input('term'), $lReq->input('term_limit'));
            DB::commit();
            UserLoanService::createLoanWeeklySchedule(Auth::user(), $loanObj->id, $lReq->input('amount'), now()->diffInWeeks($loanEndDate));

            return $this->successResponse([], __('messages.loan.accepted'));
        } catch (\Throwable$th) {
            DB::rollBack();
            logger($th);
            return $this->errorResponse();
        }
    }

    public function getAllLoans()
    {
        $loans = Auth::user()->loans()->simplePaginate(10);
        return $this->successResponse($loans);
    }
    public function getSchedule(int $loanId)
    {
        $loan = Auth::user()->loans()->findOrFail($loanId);
        $schedule = $loan->weeklySchedules()->get();
        return $this->successResponse($schedule);
    }

    public function loanPaySchedule(int $loanId, int $paymentScheduleId)
    {
        try {
            $loan = Auth::user()->loans()->findOrFail($loanId);
            $schedule = $loan->weeklySchedules()->findOrFail($paymentScheduleId);

            if ($schedule->is_paid == UserLoanWeeklySchedule::PAID) {
                $status = UserLoanWeeklySchedule::REPAID;
                $message = __('messages.loan.repaidsuccess');

            } else {
                $status = UserLoanWeeklySchedule::PAID;
                $message = __('messages.loan.success');
            }

            DB::beginTransaction();
            $schedule->is_paid = $status;
            $schedule->save();

            $loan->decrement('balance_amount', $schedule->amount);
            UserLoanPayment::create([
                'user_loan_id' => $loan->id,
                'user_id' => Auth::id(),
                'amount' => $schedule->amount,
                'remarks' => __('messages.loan.remarks'),
            ]);
            DB::commit();

            return $this->successResponse([], $message);
        } catch (\Throwable$th) {
            DB::rollBack();
            logger($th);
            return $this->errorResponse();
        }

    }
}
