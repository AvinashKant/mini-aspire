<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoanPayment extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'remarks', 'user_id', 'user_loan_id'];
}
