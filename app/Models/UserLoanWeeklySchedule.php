<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoanWeeklySchedule extends Model
{
    use HasFactory;

    protected $hidden = ['created_at', 'updated_at'];

    const PAID = 1;
    const REPAID = 1;

    public function payments()
    {
        return $this->hasOne(UserLoanPayment::class);
    }
}
