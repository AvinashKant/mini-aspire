<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoan extends Model
{
    use HasFactory;

    const PENDING = 1;
    const APPROVE = 2;
    const REJECT = 3;

    const WEEKLY = 1;
    const MONTHLY = 2;
    const YEARLY = 3;

    protected $fillable = [
        'amount', 'term', 'term_limit', 'status',
        'loan_end_date', //When admin approve the loan then loan_end_date should be updated
        'balance_amount', //When admin approve the loan then amount value should be copy in this also when ever user pay his weekly loan then this value should be updated
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function weeklySchedules()
    {
        return $this->hasMany(UserLoanWeeklySchedule::class);
    }

}
