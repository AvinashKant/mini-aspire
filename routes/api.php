<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserLoanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('tokens/create', [AuthController::class, 'getToken']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::prefix('user')->group(function () {
        Route::prefix('loan')->group(function () {
            Route::post('/', [UserLoanController::class, 'requestLoan'])->name('requestLoan');
            Route::get('/', [UserLoanController::class, 'getAllLoans'])->name('getAllLoans');
            Route::get('schedule/{loan_id}', [UserLoanController::class, 'getSchedule'])->name('getSchedule')->where('loan_id', '[0-9]+');
            Route::post('schedule/{loan_id}/pay/{payment_schedule_id}', [UserLoanController::class, 'loanPaySchedule'])->name('paySchedule')->where('loan_id', '[0-9]+')->where('payment_schedule_id', '[0-9]+');
        });
    });
});
