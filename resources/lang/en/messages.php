<?php

return [

    'login' => [
        'failed' => 'Your login credentials are incorrect.',
        'success' => 'You logged in successfully',
    ],

    'loan' => [
        'accepted' => 'Your loan request has been accepted',
        'already_paid' => 'You have already paid this schedule',
        'success' => 'You successfully paid this schedule',
        'remarks' => 'Payment done successfully.',
    ],

    'try_again' => 'Please try again after some time.',

];
